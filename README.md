This is an all-in-one PLRI board for use with a Rasberry Pi.  Unlike
other Raspberry Pi implementations of PLRI, this one does not require an
additional audio interface.  It's build onto the board.

Also unlike other implementations, strobe signal from the DTMF decoder
chip is used and therefore the 'D' code is recognized.

The PLRI circuitry is based on schematics by Kyle Yoksh (k0kn) which he
published at https://www.qsl.net/k0kn/plri.html.

The audio circuitry is based on the I2S Audio pHAT audio interface for
Raspberry Pi Zero by Sergey Kiselev.  Design files for this are at
https://github.com/skiselev/i2s_audio_phat.

For more information on PLRI, see https://en.wikipedia.org/wiki/Plri
